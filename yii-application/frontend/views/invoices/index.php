<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use dosamigos\fileinput\BootstrapFileInput;
/* @var $this yii\web\View */
/* @var $searchModel app\models\InvoicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Invoices');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoices-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-sm-6">
            <?= Html::a(Yii::t('app', 'Export'), ['export'], ['class' => 'btn btn-primary']) ?>
            <br>
            <?= $this->render('_import', [
                'model' => $model,
            ]) ?>
        </div>
        <div class="col-sm-6 text-right">
            <?= Html::a(Yii::t('app', 'Create Invoices'), ['create'], ['class' => 'btn btn-success']) ?>

        </div>
    </div>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'users',
            [
                'attribute' => 'category_id',
                'header' => 'Category',
                'filter' => \app\models\SubCategories::get(),
                'value' => function($data){
                    return \app\models\Companies::getSubCategoryName($data->company_id);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'company_id',
                'filter' => \app\models\Companies::get(),
                'value' => function($data){
                    return \app\models\Companies::getCompanyName($data->company_id);
                },
                'format' => 'raw',
            ],
            'amount',
            'taxs',
            'seller_name',
            //'price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
