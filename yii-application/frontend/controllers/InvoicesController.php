<?php

namespace frontend\controllers;

use app\models\Invoices;
use app\models\Companies;
use app\models\InvoicesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\DynamicModel;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * InvoicesController implements the CRUD actions for Invoices model.
 */
class InvoicesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Invoices models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InvoicesSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        $model = new Invoices();
        if ($this->request->isPost && $model->load($this->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->upload()) {
               // file is uploaded successfully
               $datas = \moonland\phpexcel\Excel::widget([
            		'mode' => 'import',
            		'fileName' => 'files/import.xlsx',
            		'setFirstRecordAsKeys' => true, // if you want to set the keys of record column with first record, if it not set, the header with use the alphabet column on excel.
            		'setIndexSheetByName' => true, // set this if your excel data with multiple worksheet, the index of array will be set with the sheet name. If this not set, the index will use numeric.
            	]);
                $hasError=true;
                foreach ($datas as $data) {
                    $users = $amount = $taxs = $company_id = $price = $seller_name ='';
                    if (isset($data['users'])) {
                        $users = $data['users'];
                    }
                    if (isset($data['amount'])) {
                        $amount = $data['amount'];
                    }
                    if (isset($data['taxs'])) {
                        $taxs = $data['taxs'];
                    }
                    if (isset($data['company_id'])) {
                        $company_id = $data['company_id'];
                    }
                    if (isset($data['price'])) {
                        $price = $data['price'];
                    }
                    if (isset($data['seller_name'])) {
                        $seller_name = $data['seller_name'];
                    }
                    if ($users!="" && $company_id!="" && $amount!="" && $taxs !="") {
                        $new = new Invoices;
                        $new->users= $users;
                        $new->amount= $amount;
                        $new->taxs= $taxs;
                        $new->company_id= $company_id;
                        $new->price= $price;
                        $new->seller_name= $seller_name;
                        $new->save();
                        $hasError=false;
                    }
                }
            if ($hasError) {
                \Yii::$app->session->setFlash('danger', "Error! Please import .xlsx file and check all fields.");
            }  else {
                \Yii::$app->session->setFlash('success', "Imported!");
            }
           }
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Invoices model.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionExport()
    {
        \moonland\phpexcel\Excel::export([
        	'models' => \app\models\Invoices::find()->all(),
            'savePath'=>'files',
            'format'=>'Xlsx',
        	'columns' => ['id','users','category_id','company_id','amount','taxs','seller_name','price'], //without header working, because the header will be get label from attribute label.
            'headers' => [
                'id' => 'Invoices Content',
            ],
        ]);
        \Yii::$app->session->setFlash('success', "Exported! You can see file in the folder files.");

        $searchModel = new InvoicesSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Creates a new Invoices model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Invoices();
        $companies = Companies::get();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                if (!empty($model->price)) {
                    $seller_name = $model->seller_name;
                    $model1 = DynamicModel::validateData(compact('seller_name'), [[['seller_name'], 'required']]);
                    if ($model1->hasErrors()) {
                        \Yii::$app->session->setFlash('error', "Seller name cannot be blank.");
                        return $this->redirect([Url::previous(), ['model' => $model]]);
                    }
                }
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'companies' => $companies
        ]);
    }

    /**
     * Updates an existing Invoices model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $companies = Companies::get();

        if ($this->request->isPost && $model->load($this->request->post())) {
            if (!empty($model->price)) {
                $seller_name = $model->seller_name;
                $model1 = DynamicModel::validateData(compact('seller_name'), [[['seller_name'], 'required']]);
                if ($model1->hasErrors()) {
                    \Yii::$app->session->setFlash('error', "Seller name cannot be blank.");
                    return $this->redirect([Url::previous(), ['model' => $model]]);
                }
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'companies' => $companies
        ]);
    }

    /**
     * Deletes an existing Invoices model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Invoices model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Invoices the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Invoices::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
