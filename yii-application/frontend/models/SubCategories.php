<?php

namespace app\models;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "sub_categories".
 *
 * @property int $id
 * @property int $category_id
 * @property string $title
 * @property int $created_at
 * @property int $updated_at
 */
class SubCategories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sub_categories';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'title'], 'required'],
            [['category_id', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category'),
            'title' => Yii::t('app', 'Title'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * {@inheritdoc}
     * get all categories
     */
    public function get()
    {
        $categories = self::find()->all();
        $items = ArrayHelper::map($categories,'id','title');
        return $items;
    }

    /**
     * {@inheritdoc}
     * get category name by category_id
     */
    public function getCategoryName($id)
    {
        return self::find()->where(['id'=>$id])->one()->title;
    }

    public function getList()
    {
        return ['id'=>2];
    }

}
