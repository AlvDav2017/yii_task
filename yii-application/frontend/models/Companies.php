<?php

namespace app\models;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use app\models\SubCategories;
use app\models\Categories;
use Yii;

/**
 * This is the model class for table "companies".
 *
 * @property int $id
 * @property int $sub_category_id
 * @property string $title
 * @property string|null $address
 * @property string|null $inn
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $director
 * @property int $created_at
 * @property int $updated_at
 */
class Companies extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'companies';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sub_category_id', 'title', 'address', 'inn'], 'required'],
            [['sub_category_id', 'created_at', 'updated_at'], 'integer'],
            [['email'],'email'],
            [['title', 'address', 'inn', 'phone', 'email', 'director'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sub_category_id' => Yii::t('app', 'Sub Category'),
            'title' => Yii::t('app', 'Title'),
            'address' => Yii::t('app', 'Address'),
            'inn' => Yii::t('app', 'Inn'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'director' => Yii::t('app', 'Director'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * {@inheritdoc}
     * get all categories
     */
    public function get()
    {
        $companies = self::find()->all();
        $items = ArrayHelper::map($companies,'id','title');
        return $items;
    }

    /**
     * {@inheritdoc}
     * get company name by category_id
     */
    public function getCompanyName($id)
    {
        return self::find()->where(['id'=>$id])->one()->title;
    }

    /**
     * {@inheritdoc}
     * get category name by category_id
     */
    public function getCategoryName($id)
    {
        $catId = self::find()->where(['id'=>$id])->one()->sub_category_id;
        if ($item = SubCategories::find()->where(['id'=>$catId])->one()) {
            if ($cat = Categories::find()->where(['id'=>$item->id])->one()) {
                return $cat->title;
            }
        }
        return '';
    }

    /**
     * {@inheritdoc}
     * get subcat name by category_id
     */
    public function getSubCategoryName($id){
        $catId = self::find()->where(['id'=>$id])->one()->sub_category_id;
        if ($item = SubCategories::find()->where(['id'=>$catId])->one()) {
            return $item->title;
        }
        return '';
    }
}
