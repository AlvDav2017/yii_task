<?php

namespace app\models;
use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "invoices".
 *
 * @property int $id
 * @property int $users
 * @property int $company_id
 * @property float $amount
 * @property float $taxs
 * @property string|null $seller_name
 * @property float|null $price
 */
class Invoices extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'invoices';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getcompanies()
    {
        return $this->hasMany(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['users', 'amount', 'taxs','company_id'], 'required'],
            [['users'], 'integer'],
            [['file'],'safe'],
            [['amount', 'taxs', 'price'], 'number'],
            [['seller_name'], 'string', 'max' => 255],

        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category'),
            'sub_category_id' => Yii::t('app', 'Sub Category'),
            'users' => Yii::t('app', 'Users count'),
            'company_id' => Yii::t('app', 'Company'),
            'amount' => Yii::t('app', 'Amount'),
            'taxs' => Yii::t('app', 'Taxs'),
            'seller_name' => Yii::t('app', 'Seller Name'),
            'price' => Yii::t('app', 'Price'),
        ];
    }

    public function upload()
   {
       try {
           $file = $this->file;
           $file->saveAs('files/import.xlsx');
           return true;
       } catch (\Exception $e) {
           return false;
       }

   }
}
