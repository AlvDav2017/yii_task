<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Invoices;

/**
 * InvoicesSearch represents the model behind the search form of `app\models\Invoices`.
 */
class InvoicesSearch extends Invoices
{
    public $category_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'users','category_id','company_id'], 'integer'],
            [['amount', 'taxs', 'price'], 'number'],
            [['seller_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $category_id = null)
    {
        $query = Invoices::find()->joinWith('companies');


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if($category_id){
            $this->category_id = $category_id;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'users' => $this->users,
            'amount' => $this->amount,
            'company_id' => $this->company_id,
            'taxs' => $this->taxs,
            'price' => $this->price,
            'companies.sub_category_id' => $this->category_id?:array_keys(\app\models\SubCategories::get()),
        ]);

        $query->andFilterWhere(['like', 'seller_name', $this->seller_name]);

        return $dataProvider;
    }
}
