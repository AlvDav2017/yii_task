<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%invoices}}`.
 */
class m211110_203343_create_invoices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%invoices}}', [
            'id' => $this->primaryKey(),
            'users' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'amount' => $this->float()->notNull(),
            'taxs' => $this->float()->notNull(),
            'seller_name' => $this->string()->null(),
            'price' => $this->float()->null(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%invoices}}');
    }
}
