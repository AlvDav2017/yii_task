<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%companies}}`.
 */
class m211110_173644_create_companies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%companies}}', [
            'id' => $this->primaryKey(),
            'sub_category_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'address' => $this->string()->null(),
            'inn' => $this->string()->null(),
            'phone' => $this->string()->null(),
            'email' => $this->string()->null(),
            'director' => $this->string()->null(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%companies}}');
    }
}
